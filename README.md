# PinkOrca SMS-Bomber Tool

This is a web-based tool that allows users to send multiple SMS messages to a target phone number. It is intended for educational and testing purposes only.

## [Try demo](https://pinkorca.codeberg.page/sms-bomber/@main/)
![Demo](Demo.png)



## Disclaimer

The developers of this tool are not responsible for any misuse or illegal activities performed with this tool. It is the user's responsibility to ensure that they are using this tool legally and ethically.

This tool is provided "as is" without any warranties or guarantees. The developers are not liable for any damages or consequences resulting from the use of this tool.

## Usage

1. Open the web application in your browser.
2. Enter the target phone number.
3. Click the "Bomb" button.

Note: Sending unsolicited SMS messages to individuals without their consent may be illegal in some jurisdictions. Use this tool responsibly and at your own risk.

## Contributing

If you find any issues or have suggestions for improvements, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the [GPL 3].

This script is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for details.